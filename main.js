const electron = require('electron');
const request = require('request');
const fs = require('fs');
const pReader = require('properties-reader');
const os = require('os');
const ipcMain = electron.ipcMain;

const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

const windows = {};

const properties = pReader(`${__dirname}/tyche.properties`);
const tycheManager = properties.get('tycheManager');
const httpPort = properties.get('httpPort');
const clientId = os.hostname();

const url = `http://${tycheManager}:${httpPort}/cashinsight/assure/login?clientid=${clientId}`;

let win;

function createWindow() {
  win = new BrowserWindow({
    width: 222,
    height: 552,
    maximizable: false,
    resizable: false,
    icon: 'cashinsight.ico',
    show: false,
    webPreferences: {
      preload: `${__dirname}/preload.js`,
      nodeIntegration: true
    },
    label: 'CashInsight',
    title: 'CashInsight'
  });

  win.removeMenu();
  win.loadURL(url);
  //win.openDevTools();
  win.webContents.executeJavaScript(`document.head.getElementsByTagName("title")[0].innerHTML="CashInsight";`);
  win.webContents.on;

  win.once('ready-to-show', () => {
    win.show();
  });

  win.on('closed', () => {
    win = null;
    const closeUrl = `http://${tycheManager}:${httpPort}/cashinsight/assure/doTransaction?transaction=transaction_logout?clientid=${clientId}&action=INIT&field_is_exit=1`;
    request.get(closeUrl, function (error, response, body) {
      //console.log(error);
      //console.log(body);
    });
  });
};

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if(process.platofrm !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if(win === null) {
    createWindow();
  }
});

ipcMain.on('hello', function (event, arg) {
  if (windows[arg.transaction] == null || windows[arg.transaction] === 'undefined') {

    const txWindow = new BrowserWindow({
      maximizable: false,
      resizable: false,
      show: false,
      width: 800,
      height: 552,
      icon: 'cashinsight.ico',
      webPreferences: {
        preload: `${__dirname}/renderer.js`,
        nodeIntegration: true
      }
    });

    windows[arg.transaction] = txWindow;

    const txURL = `http://${tycheManager}:${httpPort}/cashinsight/assure/doTransaction?transaction=${arg.transaction}&clientid=${arg.clientid}&action=START`;

    txWindow.removeMenu();
    txWindow.loadURL(txURL);
    //txWindow.openDevTools();

    txWindow.once('ready-to-show', () => {
      txWindow.show();
    });

    ipcMain.on('close', function (event, arg) {
      windows[arg].close();
    });

    windows[arg.transaction].on('closed', () => {
      windows[arg.transaction] = null;
    });
  }
});