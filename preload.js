const ipcRenderer = require('electron').ipcRenderer;
window.ipcRenderer = ipcRenderer;

function openTxWindow(windowId, clientId) {

  var _data = {
    transaction: windowId,
    clientid: clientId
  };

  window.ipcRenderer.send('hello', _data);
};

window.openTxWindow = openTxWindow;