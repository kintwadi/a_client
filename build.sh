#!/bin/bash
echo "Assure electron-client build process - Start"

if [ $1 == 'yes' ]; then

    echo "Preparing dependencies..."
    command npm install &&
    command npm install electron --save-dev && 
    command npm install electron-packager --save-dev 

fi

# build for Windows platform
if [ $2 == 'win' ]; then

   echo " Building packages for Window ..."
   command npm run build-win

fi
# build for Linux platform
if [ $2 == 'linux' ]; then

   echo " Building packages for Linux ..."
   command npm run build-linux
fi

# build for Mac OS platform
if [ $2 == 'mac' ]; then

   echo " Building packages for Mac-OS ..."
   command npm run build-mac
fi

# build for all platforms
if [ $2 == 'all' ]; then

   echo " Building for all platforms package..."
   command npm run build-all
fi

echo "assure electron-client build process - End"
