const ipcRenderer = require('electron').ipcRenderer;
window.ipcRenderer = ipcRenderer;

function closeWindow(transactionId) {
  setTimeout(() => { window.ipcRenderer.send('close', transactionId) }, 100 );
};

window.closeWindow = closeWindow;